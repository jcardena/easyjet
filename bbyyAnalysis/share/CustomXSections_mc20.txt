dataset_number/I:physics_short/C:crossSection_pb/D:kFactor/D:genFiltEff/D:relUncertUP/D:relUncertDOWN/D
600021  PhPy8EG_PDF4LHC15_HHbbyy_cHHH01d0                          8.20992E-05  1.0  1.0  0.0  0.0
600022  PhPy8EG_PDF4LHC15_HHbbyy_cHHH10d0                          1.71073E-03  1.0  1.0  0.0  0.0
603559  PhPy8EG_PDF4LHC21_HHbbyy_chhh1p0                           8.20992E-05  1.0  1.0  0.0  0.0
603558  PhPy8EG_PDF4LHC21_HHbbyy_chhh0p0                           1.82892E-04  1.0  1.0  0.0  0.0
603559  PhPy8EG_PDF4LHC21_HHbbyy_chhh1p0                           8.20992E-05  1.0  1.0  0.0  0.0
603560  PhPy8EG_PDF4LHC21_HHbbyy_chhh5p0                           2.42279E-04  1.0  1.0  0.0  0.0
603695  PhPy8EG_PDF4LHC21_HHbbyy_chhh2p5                           3.64588E-05  1.0  1.0  0.0  0.0
603696  PhPy8EG_PDF4LHC21_HHbbyy_chhh10p0                          1.71073E-03  1.0  1.0  0.0  0.0
603697  PhPy8EG_PDF4LHC21_HHbbyy_chhhm1p0                          3.40031E-04  1.0  1.0  0.0  0.0
503004  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1cv1                        4.46059E-06  1.0  1.0  0.0  0.0
503007  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1p5cv1                      8.38707E-06  1.0  1.0  0.0  0.0
503011  MGPy8EG_hh_bbyy_vbf_novhh_l2cvv1cv1                        4.03225E-06  1.0  1.0  0.0  0.0
503012  MGPy8EG_hh_bbyy_vbf_novhh_l10cvv1cv1                       2.46937E-04  1.0  1.0  0.0  0.0
503013  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1cv0p5                      2.72263E-05  1.0  1.0  0.0  0.0
508689  MGPy8EG_hh_bbyy_vbf_novhh_lm5cvv1cv0p5                     1.62004E-05  1.0  1.0  0.0  0.0
525376  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1cv1_5fns                   4.46059E-06  1.0  1.0  0.0  0.0
525377  MGPy8EG_hh_bbyy_vbf_novhh_l0cvv1cv1_5fns                   1.17054E-05  1.0  1.0  0.0  0.0
525378  MGPy8EG_hh_bbyy_vbf_novhh_l2cvv1cv1_5fns                   4.03225E-06  1.0  1.0  0.0  0.0
525379  MGPy8EG_hh_bbyy_vbf_novhh_l10cvv1cv1_5fns                  2.46937E-04  1.0  1.0  0.0  0.0
525380	MGPy8EG_hh_bbyy_vbf_novhh_l1cvv0cv1_5fns                   6.77999E-05  1.0  1.0  0.0  0.0
525381	MGPy8EG_hh_bbyy_vbf_novhh_l1cvv0p5cv1_5fns                 2.42185E-05  1.0  1.0  0.0  0.0
525382	MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1p5cv1_5fns                 8.38707E-06  1.0  1.0  0.0  0.0
525383  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv2cv1_5fns                   3.60417E-05  1.0  1.0  0.0  0.0
525384  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv3cv1_5fns                   1.62273E-04  1.0  1.0  0.0  0.0
525385  MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1cv0p5_5fns                 2.72263E-05  1.0  1.0  0.0  0.0
525386	MGPy8EG_hh_bbyy_vbf_novhh_l1cvv1cv1p5_5fns                 1.66030E-04  1.0  1.0  0.0  0.0
525387	MGPy8EG_hh_bbyy_vbf_novhh_l0cvv0cv1_5fns                   9.25195E-05  1.0  1.0  0.0  0.0
525388	MGPy8EG_hh_bbyy_vbf_novhh_lm5cvv1cv0p5_5fns                1.62004E-05  1.0  1.0  0.0  0.0
343981  PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam           1.10277E-01  1.0  1.0  0.0  0.0
346214  PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_gamgam            8.58514E-03  1.0  1.0  0.0  0.0
345318  PowhegPythia8EvtGen_NNPDF30_AZNLO_WpH125J_Hyy_Wincl_MINLO  1.90680E-03  1.0  1.0  0.0  0.0
345317  PowhegPythia8EvtGen_NNPDF30_AZNLO_WmH125J_Hyy_Wincl_MINLO  1.20946E-03  1.0  1.0  0.0  0.0
345319  PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Hyy_Zincl_MINLO   1.72792E-03  1.0  1.0  0.0  0.0
345061  PowhegPythia8EvtGen_NNPDF3_AZNLO_ggZH125_HgamgamZinc       2.78529E-04  1.0  1.0  0.0  0.0
345315  PowhegPythia8EvtGen_A14NNPDF23LO_bbHyy                     1.10776E-03   1.0  1.0  0.0  0.0
346525  PowhegPythia8EvtGen_A14NNPDF23_NNPDF30ME_ttH125_gamgam     1.15112E-03  1.0  1.0  0.0  0.0
346677  aMcAtNloPythia8EvtGen_tHjb125_4fl_CPalpha_0_gamgam         1.68548E-04  1.0  1.0  0.0  0.0
346759  aMcAtNloPythia8EvtGen_tWH125_CPalpha_0_gamgam              3.44359E-05  1.0  1.0  0.0  0.0
508781  MGPy8EG_ttyy_nonallhad_LO                                  6.4820E-03   1.0  1.0  0.0  0.0
800904  Py8EG_A14NNPDF23LO_XHS_X170_S030_HyySbb                    1.0E-03      1.0  1.0  0.0  0.0
800913  Py8EG_A14NNPDF23LO_XHS_X245_S090_HyySbb                    1.0E-03      1.0  1.0  0.0  0.0
800915  Py8EG_A14NNPDF23LO_XHS_X205_S030_HyySbb                    1.0E-03      1.0  1.0  0.0  0.0
801367  Py8EG_A14NNPDF23LO_XHS_X750_S170_HyySbb                    1.0E-03      1.0  1.0  0.0  0.0
801370  Py8EG_A14NNPDF23LO_XHS_X750_S400_HyySbb                    1.0E-03      1.0  1.0  0.0  0.0
